package com.example.bukuwarungassignment

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class BukuWarungApplication : Application() {
}