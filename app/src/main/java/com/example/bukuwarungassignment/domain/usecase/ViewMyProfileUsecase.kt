package com.example.bukuwarungassignment.domain.usecase

import com.example.bukuwarungassignment.data.dataStore.PreferenceManager
import kotlinx.coroutines.flow.Flow

class ViewMyProfileUsecase(
    private val preferenceManager: PreferenceManager
) {
    fun getName(): Flow<String?> {
        return preferenceManager.getName()
    }

    fun getEmail(): Flow<String?> {
        return preferenceManager.getEmail()
    }

    fun getProfilePic(): Flow<String?> {
        return preferenceManager.getProfilePic()
    }
}