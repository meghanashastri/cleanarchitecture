package com.example.bukuwarungassignment.domain.usecase

import com.example.bukuwarungassignment.data.model.Data
import com.example.bukuwarungassignment.data.utils.Resource
import com.example.bukuwarungassignment.domain.repository.UsersRepository
import kotlinx.coroutines.flow.Flow

class GetUsersUsecase(private val usersRepository: UsersRepository) {

    fun execute(): Flow<Resource<List<Data>>?> {
        return usersRepository.getUsers()
    }
}