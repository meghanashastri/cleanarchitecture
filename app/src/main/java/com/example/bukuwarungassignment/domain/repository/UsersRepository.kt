package com.example.bukuwarungassignment.domain.repository

import com.example.bukuwarungassignment.data.model.Data
import com.example.bukuwarungassignment.data.utils.Resource
import kotlinx.coroutines.flow.Flow

interface UsersRepository {
    fun getUsers(): Flow<Resource<List<Data>>?>
}