package com.example.bukuwarungassignment.data.repository.datasource

import com.example.bukuwarungassignment.data.model.UserApiResponse
import retrofit2.Response

interface UserRemoteDataSource {
    suspend fun getUsers(): Response<UserApiResponse>
}