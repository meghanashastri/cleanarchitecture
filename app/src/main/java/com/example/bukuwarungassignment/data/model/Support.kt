package com.example.bukuwarungassignment.data.model

data class Support(
    val text: String,
    val url: String
)