package com.example.bukuwarungassignment.data.repository

import com.example.bukuwarungassignment.data.dataStore.PreferenceManager
import com.example.bukuwarungassignment.data.model.UserApiResponse
import com.example.bukuwarungassignment.data.repository.datasource.UserLocalDataSource
import com.example.bukuwarungassignment.data.repository.datasource.UserRemoteDataSource
import com.example.bukuwarungassignment.data.utils.Resource
import com.example.bukuwarungassignment.data.utils.networkBoundResource
import com.example.bukuwarungassignment.domain.repository.UsersRepository
import kotlinx.coroutines.delay
import retrofit2.Response

class UsersRepositoryImpl(
    private val userRemoteDataSource: UserRemoteDataSource,
    private val userLocalDataSource: UserLocalDataSource,
    private val preferenceManager: PreferenceManager
) : UsersRepository {

    override fun getUsers() = networkBoundResource(
        query = {
            userLocalDataSource.getUsers()
        },
        fetch = {
            preferenceManager.saveName("Temperance Brennan")
            preferenceManager.saveEmail("bones@gmail.com")
            preferenceManager.saveProfilePic("https://reqres.in/img/faces/1-image.jpg")
            delay(2000)
            responseToResource(userRemoteDataSource.getUsers())
        },
        saveFetchResult = { users ->
            users.data?.data?.let { userLocalDataSource.deleteAndInsertUsers(it) }
        }
    )

    private fun responseToResource(response: Response<UserApiResponse>): Resource<UserApiResponse> {
        if (response.isSuccessful) {
            response.body()?.let { result ->
                return Resource.Success(result)
            }
        }
        return Resource.Error(response.message())
    }
}