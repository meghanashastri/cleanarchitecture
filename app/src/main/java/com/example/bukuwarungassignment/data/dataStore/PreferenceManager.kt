package com.example.bukuwarungassignment.data.dataStore

import kotlinx.coroutines.flow.Flow

interface PreferenceManager {

    suspend fun saveName(name: String)
    fun getName(): Flow<String?>
    suspend fun saveEmail(email: String)
    fun getEmail(): Flow<String?>
    suspend fun saveProfilePic(pic: String)
    fun getProfilePic(): Flow<String?>

}