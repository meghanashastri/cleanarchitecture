package com.example.bukuwarungassignment.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.bukuwarungassignment.data.model.Data

@Database(
    entities = [Data::class],
    version = 1,
    exportSchema = false
)
abstract class Database : RoomDatabase() {
    abstract fun getUserDAO(): UserDao
}
