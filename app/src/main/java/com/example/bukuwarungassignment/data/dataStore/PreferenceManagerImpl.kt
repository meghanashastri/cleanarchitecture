package com.example.bukuwarungassignment.data.dataStore

import android.content.Context
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.stringPreferencesKey
import androidx.datastore.preferences.preferencesDataStore
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class PreferenceManagerImpl @Inject constructor(
    @ApplicationContext private val context: Context
) : PreferenceManager {

    private val Context.userDataStore by preferencesDataStore(USER_PREFS)

    companion object {
        private const val USER_PREFS = "user_prefs"
        private val NAME = stringPreferencesKey(name = "name")
        private val EMAIL = stringPreferencesKey(name = "email")
        private val PROFILE_PIC = stringPreferencesKey(name = "profile_pic")
    }

    override suspend fun saveName(name: String) {
        context.userDataStore.edit { preferences ->
            preferences[NAME] = name
        }
    }

    override fun getName(): Flow<String?> {
        return context.userDataStore.data
            .map { preferences ->
                preferences[NAME] ?: ""
            }
    }

    override suspend fun saveEmail(email: String) {
        context.userDataStore.edit { preferences ->
            preferences[EMAIL] = email
        }
    }

    override fun getEmail(): Flow<String?> {
        return context.userDataStore.data
            .map { preferences ->
                preferences[EMAIL] ?: ""
            }
    }

    override suspend fun saveProfilePic(pic: String) {
        context.userDataStore.edit { preferences ->
            preferences[PROFILE_PIC] = pic
        }
    }

    override fun getProfilePic(): Flow<String?> {
        return context.userDataStore.data
            .map { preferences ->
                preferences[PROFILE_PIC] ?: ""
            }
    }
}