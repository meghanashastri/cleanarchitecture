package com.example.bukuwarungassignment.data.repository.datasourceImpl

import com.example.bukuwarungassignment.data.api.UserApiService
import com.example.bukuwarungassignment.data.model.UserApiResponse
import com.example.bukuwarungassignment.data.repository.datasource.UserRemoteDataSource
import retrofit2.Response

class UserRemoteDataSourceImpl(
    private val userApiService: UserApiService
) : UserRemoteDataSource {
    override suspend fun getUsers(): Response<UserApiResponse> {
        return userApiService.getUsers()
    }
}