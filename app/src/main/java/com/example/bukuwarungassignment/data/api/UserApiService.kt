package com.example.bukuwarungassignment.data.api

import com.example.bukuwarungassignment.data.model.UserApiResponse
import retrofit2.Response
import retrofit2.http.GET

interface UserApiService {

    @GET("users")
    suspend fun getUsers(): Response<UserApiResponse>
}