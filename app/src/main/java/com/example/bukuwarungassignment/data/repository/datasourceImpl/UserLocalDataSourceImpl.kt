package com.example.bukuwarungassignment.data.repository.datasourceImpl

import androidx.room.withTransaction
import com.example.bukuwarungassignment.data.db.Database
import com.example.bukuwarungassignment.data.db.UserDao
import com.example.bukuwarungassignment.data.model.Data
import com.example.bukuwarungassignment.data.repository.datasource.UserLocalDataSource
import kotlinx.coroutines.flow.Flow

class UserLocalDataSourceImpl(
    private val db: Database,
    private val usersDao: UserDao
) : UserLocalDataSource {

    override fun getUsers(): Flow<List<Data>> {
        return usersDao.getAllUsers()
    }

    override suspend fun deleteAllUsers() {
        usersDao.deleteAllUsers()
    }

    override suspend fun insertUsers(users: List<Data>) {
        usersDao.insertUsers(users)
    }

    override suspend fun deleteAndInsertUsers(users: List<Data>) {
        db.withTransaction {
            deleteAllUsers()
            insertUsers(users)
        }
    }
}