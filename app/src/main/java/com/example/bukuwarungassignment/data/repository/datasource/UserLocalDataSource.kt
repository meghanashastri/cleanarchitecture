package com.example.bukuwarungassignment.data.repository.datasource

import com.example.bukuwarungassignment.data.model.Data
import kotlinx.coroutines.flow.Flow

interface UserLocalDataSource {
    fun getUsers(): Flow<List<Data>>

    suspend fun deleteAllUsers()

    suspend fun insertUsers(users: List<Data>)

    suspend fun deleteAndInsertUsers(users: List<Data>)
}