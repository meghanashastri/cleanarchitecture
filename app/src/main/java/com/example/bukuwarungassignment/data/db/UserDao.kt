package com.example.bukuwarungassignment.data.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.bukuwarungassignment.data.model.Data
import kotlinx.coroutines.flow.Flow

@Dao
interface UserDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertUsers(users: List<Data>)

    @Query("SELECT * FROM users")
    fun getAllUsers():Flow<List<Data>>

    @Query("DELETE FROM users")
    suspend fun deleteAllUsers()
}