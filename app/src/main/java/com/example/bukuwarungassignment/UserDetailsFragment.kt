package com.example.bukuwarungassignment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.example.bukuwarungassignment.data.model.Data
import com.example.bukuwarungassignment.databinding.FragmentUserDeatilsBinding

class UserDetailsFragment : Fragment() {

    private lateinit var fragmentUserDetailsBinding: FragmentUserDeatilsBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_user_deatils, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        fragmentUserDetailsBinding = FragmentUserDeatilsBinding.bind(view)
        val args: UserDetailsFragmentArgs by navArgs()
        val user = args.selectedUser
        setData(user)
    }

    private fun setData(user: Data) {
        fragmentUserDetailsBinding.apply {
            tvId.text = getString(R.string.id, user.id.toString())
            tvFirstName.text = getString(R.string.first_name, user.first_name)
            tvLastName.text = getString(R.string.last_name, user.last_name)
            tvEmail.text = getString(R.string.email, user.email)
            Glide.with(ivProfilePic.context)
                .load(user.avatar)
                .circleCrop()
                .into(ivProfilePic)
        }
    }
}