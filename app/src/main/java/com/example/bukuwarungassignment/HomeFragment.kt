package com.example.bukuwarungassignment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.bukuwarungassignment.data.utils.Resource
import com.example.bukuwarungassignment.databinding.FragmentHomeBinding
import com.example.bukuwarungassignment.presentation.adapter.UserListAdapter
import com.example.bukuwarungassignment.presentation.viewModel.UsersViewModel

class HomeFragment : Fragment() {
    private lateinit var viewModel: UsersViewModel
    private lateinit var fragmentHomeBinding: FragmentHomeBinding
    private lateinit var userListAdapter: UserListAdapter
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        fragmentHomeBinding = FragmentHomeBinding.bind(view)
        viewModel = (activity as MainActivity).viewModel
        userListAdapter = (activity as MainActivity).userListAdapter
        navigateToDetailsFragment()
        initRecyclerView()
        viewUserList()
    }

    private fun navigateToDetailsFragment() {
        userListAdapter.setOnItemClickListener {
            val bundle = Bundle().apply {
                putSerializable("selected_user", it)
            }
            findNavController().navigate(
                R.id.action_homeFragment2_to_userDeatilsFragment,
                bundle
            )
        }
    }

    private fun initRecyclerView() {
        fragmentHomeBinding.apply {
            recyclerViewUserList.adapter = userListAdapter
            recyclerViewUserList.layoutManager = LinearLayoutManager(activity)
        }
    }

    private fun viewUserList() {
        viewModel.users.observe(viewLifecycleOwner) { result ->
            userListAdapter.differ.submitList(result?.data)

            fragmentHomeBinding.apply {
                progressBar.isVisible =
                    result is Resource.Loading && result.data.isNullOrEmpty()

                tvError.isVisible =
                    result is Resource.Error && result.data.isNullOrEmpty()
                tvError.text = result?.message
            }
        }
    }
}