package com.example.bukuwarungassignment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.example.bukuwarungassignment.databinding.FragmentProfileBinding
import com.example.bukuwarungassignment.presentation.viewModel.UsersViewModel

class ProfileFragment : Fragment() {
    private lateinit var fragmentProfileBinding: FragmentProfileBinding
    private lateinit var viewModel: UsersViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_profile, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        fragmentProfileBinding = FragmentProfileBinding.bind(view)
        viewModel = (activity as MainActivity).viewModel
        showUserData()
    }

    private fun showUserData() {
        fragmentProfileBinding.apply {
            viewModel.name.observe(viewLifecycleOwner) { name ->
                tvName.text = getString(R.string.name, name)
            }

            viewModel.email.observe(viewLifecycleOwner) { email ->
                tvEmail.text = getString(R.string.email, email)
            }

            viewModel.profilePic.observe(viewLifecycleOwner) { pic ->
                Glide.with(ivProfilePic.context)
                    .load(pic)
                    .circleCrop()
                    .into(ivProfilePic)
            }
        }
    }
}