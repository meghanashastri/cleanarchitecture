package com.example.bukuwarungassignment.presentation.di

import com.example.bukuwarungassignment.data.api.UserApiService
import com.example.bukuwarungassignment.data.repository.datasource.UserRemoteDataSource
import com.example.bukuwarungassignment.data.repository.datasourceImpl.UserRemoteDataSourceImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class RemoteDataSourceModule {

    @Singleton
    @Provides
    fun provideUserRemoteDataSource(userApiService: UserApiService): UserRemoteDataSource {
        return UserRemoteDataSourceImpl(userApiService)
    }
}