package com.example.bukuwarungassignment.presentation.viewModel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.bukuwarungassignment.domain.usecase.GetUsersUsecase
import com.example.bukuwarungassignment.domain.usecase.ViewMyProfileUsecase

class UsersViewModelFactory(
    private val getUsers: GetUsersUsecase,
    private val viewUser: ViewMyProfileUsecase
) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return UsersViewModel(
            getUsers,
            viewUser
        ) as T
    }
}