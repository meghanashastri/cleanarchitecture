package com.example.bukuwarungassignment.presentation.di

import com.example.bukuwarungassignment.data.db.Database
import com.example.bukuwarungassignment.data.db.UserDao
import com.example.bukuwarungassignment.data.repository.datasource.UserLocalDataSource
import com.example.bukuwarungassignment.data.repository.datasourceImpl.UserLocalDataSourceImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class LocalDataSourceModule {

    @Singleton
    @Provides
    fun provideUserLocalDataSource(db: Database, userDao: UserDao): UserLocalDataSource {
        return UserLocalDataSourceImpl(db, userDao)
    }
}