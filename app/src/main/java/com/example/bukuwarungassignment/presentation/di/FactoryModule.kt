package com.example.bukuwarungassignment.presentation.di

import com.example.bukuwarungassignment.domain.usecase.GetUsersUsecase
import com.example.bukuwarungassignment.domain.usecase.ViewMyProfileUsecase
import com.example.bukuwarungassignment.presentation.viewModel.UsersViewModelFactory
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class FactoryModule {

    @Singleton
    @Provides
    fun provideUserViewModelFactory(
        getUsersUsecase: GetUsersUsecase,
        viewMyProfileUsecase: ViewMyProfileUsecase
    ): UsersViewModelFactory {
        return UsersViewModelFactory(
            getUsersUsecase,
            viewMyProfileUsecase
        )
    }
}