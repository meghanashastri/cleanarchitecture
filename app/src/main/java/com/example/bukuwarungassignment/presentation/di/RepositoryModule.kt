package com.example.bukuwarungassignment.presentation.di

import com.example.bukuwarungassignment.data.dataStore.PreferenceManager
import com.example.bukuwarungassignment.data.repository.UsersRepositoryImpl
import com.example.bukuwarungassignment.data.repository.datasource.UserLocalDataSource
import com.example.bukuwarungassignment.data.repository.datasource.UserRemoteDataSource
import com.example.bukuwarungassignment.domain.repository.UsersRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class RepositoryModule {

    @Singleton
    @Provides
    fun provideUserRepository(
        userRemoteDataSource: UserRemoteDataSource,
        userLocalDataSource: UserLocalDataSource,
        preferenceManager: PreferenceManager
    ): UsersRepository {
        return UsersRepositoryImpl(userRemoteDataSource, userLocalDataSource, preferenceManager)
    }
}