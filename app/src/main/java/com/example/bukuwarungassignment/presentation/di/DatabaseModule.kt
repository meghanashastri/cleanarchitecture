package com.example.bukuwarungassignment.presentation.di

import android.app.Application
import androidx.room.Room
import com.example.bukuwarungassignment.data.db.Database
import com.example.bukuwarungassignment.data.db.UserDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class DatabaseModule {

    @Singleton
    @Provides
    fun provideDatabase(app: Application): Database {
        return Room.databaseBuilder(app, Database::class.java, "bukuWarung_db")
            .fallbackToDestructiveMigration()
            .build()
    }

    @Singleton
    @Provides
    fun provideUserDao(db: Database): UserDao {
        return db.getUserDAO()
    }
}