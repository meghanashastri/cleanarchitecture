package com.example.bukuwarungassignment.presentation.di

import com.example.bukuwarungassignment.data.dataStore.PreferenceManager
import com.example.bukuwarungassignment.domain.repository.UsersRepository
import com.example.bukuwarungassignment.domain.usecase.GetUsersUsecase
import com.example.bukuwarungassignment.domain.usecase.ViewMyProfileUsecase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class UseCaseModule {

    @Singleton
    @Provides
    fun provideGetUsersUseCase(
        usersRepository: UsersRepository
    ): GetUsersUsecase {
        return GetUsersUsecase(usersRepository)
    }

    @Singleton
    @Provides
    fun provideViewUserUseCase(
        preferenceManager: PreferenceManager
    ): ViewMyProfileUsecase {
        return ViewMyProfileUsecase(preferenceManager)
    }
}