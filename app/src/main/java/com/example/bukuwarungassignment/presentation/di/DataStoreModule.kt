package com.example.bukuwarungassignment.presentation.di

import com.example.bukuwarungassignment.data.dataStore.PreferenceManager
import com.example.bukuwarungassignment.data.dataStore.PreferenceManagerImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class DataStoreModule {

    @Singleton
    @Binds
    abstract fun bindDataStore(impl: PreferenceManagerImpl): PreferenceManager

}