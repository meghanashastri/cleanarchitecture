package com.example.bukuwarungassignment.presentation.di

import com.example.bukuwarungassignment.presentation.adapter.UserListAdapter
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class AdapterModule {

    @Singleton
    @Provides
    fun provideUserAdapter(): UserListAdapter {
        return UserListAdapter()
    }
}