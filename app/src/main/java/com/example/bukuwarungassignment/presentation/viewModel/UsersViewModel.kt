package com.example.bukuwarungassignment.presentation.viewModel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import com.example.bukuwarungassignment.domain.usecase.GetUsersUsecase
import com.example.bukuwarungassignment.domain.usecase.ViewMyProfileUsecase

class UsersViewModel(
    private val getUsers: GetUsersUsecase,
    private val viewUser: ViewMyProfileUsecase
) : ViewModel() {
    val users = getUsers.execute().asLiveData()

    val name = viewUser.getName().asLiveData()
    val email = viewUser.getEmail().asLiveData()
    val profilePic = viewUser.getProfilePic().asLiveData()
}