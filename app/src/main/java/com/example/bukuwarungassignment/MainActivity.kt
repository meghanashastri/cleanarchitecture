package com.example.bukuwarungassignment

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.view.isVisible
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupWithNavController
import com.example.bukuwarungassignment.databinding.ActivityMainBinding
import com.example.bukuwarungassignment.presentation.adapter.UserListAdapter
import com.example.bukuwarungassignment.presentation.viewModel.UsersViewModel
import com.example.bukuwarungassignment.presentation.viewModel.UsersViewModelFactory
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    @Inject
    lateinit var factory: UsersViewModelFactory

    @Inject
    lateinit var userListAdapter: UserListAdapter
    lateinit var viewModel: UsersViewModel
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        init()
    }

    private fun init() {
        val navHostFragment =
            supportFragmentManager.findFragmentById(R.id.fragmentContainerView) as NavHostFragment
        val navController = navHostFragment.navController
        binding.apply {
            bottomNavigation.setupWithNavController(
                navController
            )

            navController.addOnDestinationChangedListener { _, _, arguments ->
                bottomNavigation.isVisible =
                    arguments?.getBoolean("show_nav_bar", false) == true
            }
        }

        viewModel = ViewModelProvider(this, factory)
            .get(UsersViewModel::class.java)
    }

}